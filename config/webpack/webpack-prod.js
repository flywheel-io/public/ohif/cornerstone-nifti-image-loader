const webpack = require('webpack');
const merge = require('./merge');
const baseConfig = require('./webpack-base');

const prodConfig = {
  output: {
    filename: '[name].min.js'
  },
  plugins: [
  ],
  optimization: {
      minimize: true, 
    }
};

module.exports = merge(baseConfig, prodConfig);
